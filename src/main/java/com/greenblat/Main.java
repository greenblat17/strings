package com.greenblat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите любую строку: ");
        String in = scanner.nextLine();
        char[] line = in.toCharArray();


        // 1
        System.out.println("1. Change 'object-oriented programming' with  'OOP'");

        String[] searchValue = ("object-oriented programming").split(" ");
        String insertValue = "OOP";

        StringBuilder result = new StringBuilder();
        StringBuilder currentWord = new StringBuilder();
        String lastWord = "";
        int countInsertValue = 0;
        int idx = 0;
        while (idx < line.length) {
            if (line[idx] != ' ' && line[idx] != '.' && line[idx] != ',') {
                currentWord.append(line[idx]);
            } else {
                String s = currentWord.toString();
                if (s.equals(searchValue[1])) {
                    if (lastWord.toLowerCase().equals(searchValue[0])) {
                        if (countInsertValue % 2 == 1)
                            result.append(insertValue).append(line[idx]);
                        else
                            result.append(lastWord).append(" ").append(currentWord).append(line[idx]);
                        countInsertValue++;
                    } else {
                        result.append(lastWord).append(" ").append(currentWord).append(line[idx]);
                    }
                } else if (!s.toLowerCase().equals(searchValue[0])) {
                    result.append(s).append(line[idx]);
                }
                lastWord = s;
                currentWord.setLength(0);
            }

            idx++;
        }
        System.out.println(result.append(currentWord));


        System.out.println("------------------------------------");


        // 2
        System.out.println("2. Find a word in which the number of different characters is minimal");

        // Solution with array
        String[] words = result.toString().split(" ");

        boolean[] symbols = new boolean[128];
        int min = Integer.MAX_VALUE;
        String res = "";
        for (String word : words) {
            int count = 0;
            for (int j = 0; j < word.length(); j++) {
                char symbol = word.charAt(j);

                if (symbol == '.' || symbol == ',' || symbol == '"' || symbol == '(' || symbol == ')')
                    continue;

                // знак тир, делящее два слова
                if (symbol == '-') {
                    if (count < min) {
                        min = count;
                        res = word;
                    }
                    count = 0;
                    Arrays.fill(symbols, false);
                }

                if (!symbols[symbol])
                    count++;
                symbols[symbol] = true;
            }

            if (count < min) {
                min = count;
                res = word;
            }
            Arrays.fill(symbols, false);
        }
        System.out.println(res);

        // Solution with Set
        Set<Character> set = new HashSet<>();
        min = Integer.MAX_VALUE;
        res = "";
        for (String word : words) {
            int count = 0;
            for (int j = 0; j < word.length(); j++) {
                char symbol = word.charAt(j);

                if (symbol == '.' || symbol == ',' || symbol == '"' || symbol == '(' || symbol == ')')
                    continue;

                // знак тир, делящее два слова
                if (symbol == '-') {
                    if (count < min) {
                        min = count;
                        res = word;
                    }
                    count = 0;
                    set.clear();
                }

                if (!set.contains(symbol)) {
                    set.add(symbol);
                    count++;
                }
            }

            if (count < min) {
                min = count;
                res = word;
            }
            set.clear();
        }
        System.out.println(res);


        System.out.println("------------------------------------");

        // 3
        System.out.println("3. Find the number of words containing only latin characters");

        int countWordWithLatinLetters = 0;
        for (String word : words) {
            boolean isLatinLetters = true;
            for (int j = 0; j < word.length(); j++) {
                char symbol = word.charAt(j);

                if (symbol == '.' || symbol == ',' || symbol == '"' || symbol == '(' || symbol == ')')
                    continue;

                if (symbol == '-') {
                    countWordWithLatinLetters++;
                    continue;
                }

                if (!(('a' <= symbol && symbol <= 'z') || ('A' <= symbol && symbol <= 'Z'))) {
                    isLatinLetters = false;
                    break;
                }
            }

            if (isLatinLetters)
                countWordWithLatinLetters++;
        }
        System.out.println(countWordWithLatinLetters);


        System.out.println("------------------------------------");

        // 4
        System.out.println("4. Find words palindromes");
        for (String word : words) {
            int start = 0;
            int end = word.length() - 1;
            boolean isPalindrome = true;
            while (start < end) {
                if (word.charAt(start) != word.charAt(end)) {
                    isPalindrome = false;
                    break;
                }
                start++;
                end--;
            }

            if (isPalindrome)
                System.out.println(word);
        }
    }
}